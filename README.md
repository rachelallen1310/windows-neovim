# required
Move-Item $env:LOCALAPPDATA\nvim $env:LOCALAPPDATA\nvim.bak

# optional but recommended
Move-Item $env:LOCALAPPDATA\nvim-data $env:LOCALAPPDATA\nvim-data.bak

# required
git clone https://gitlab.com/rachelallen1310/windows-neovim.git $env:LOCALAPPDATA\nvim


# Remove git so you can add it to your own repo

Remove-Item $env:LOCALAPPDATA\nvim\.git -Recurse -Force

# Start NeoVim

nvim
